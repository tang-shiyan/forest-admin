import Vue from 'vue'
import Router from 'vue-router'
// import {generalRoutes} from './generalRouter.js'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'
 
// 最高权限路由
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/auth-redirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: '/dashboard',
        component: () => import('@/views/dashboard/index'),
        name: 'Dashboard',
        meta: { title: '首页', icon: 'dashboard', affix: true }
      },
    ]
  },
  {
    path: '/work',
    component: Layout,
    redirect: '/table/complex-table',
    name: 'work',
    meta: {
      title: '随手拍',
      icon: 'table'
    },
    children: [

        {
          path: 'Urgent-task',
          component: () => import('@/views/table/Urgent-task'),
          name: 'InlineEditTable',
          meta: { title: '随手拍' ,icon: 'example',}
        },
      ]
    },
    {
      path: '/Integral',
      component: Layout,
      redirect: '/table/Integral',
      name: 'Integral',
      meta: {
        title: '随手拍',
        icon: 'table'
      },
      children: [
  
          {
            path: 'Integral',
            component: () => import('@/views/table/Integral'),
            name: 'Integral',
            meta: { title: '积分列表' ,icon: 'list',}
          },
        ]
      },
    //跟下面一样可用
  // {
  //   path: '/manage',
  //   component: Layout,
  //   redirect: '/permission/page',
  //   name: 'manage',
  //   meta: {
  //     title: '商品信息',
  //     icon: 'table'
  //   },
  //   children: [

  //       {
  //         path: 'travel-search',
  //         component: () => import('@/views/permission/travel-search'),
  //         name: 'travelsearch',
  //         meta: { title: '商品信息' ,icon: 'star',}
  //       },
  //       {
  //         path: 'check-daily',
  //         component: () => import('@/views/table/check-daily'),
  //         name: 'checkdaily',
  //         meta: { title: '下单列表' ,icon: 'tab',}
  //       },
  //     ]
  // },
  {//商品信息
    path: '/manage',
    component: Layout,
    redirect: '/permission/page',
    alwaysShow: true, 
    name: 'manage',
    meta: {
      title: '商品信息',
      icon: 'guide',
    },
    children: [
      {//日常工作
        path: 'travel-search',
        component: () => import('@/views/permission/travel-search'),
        name: 'travelsearch',
        meta: {
          title: '商品列表',
          icon: 'star',
        }
      },
      {
        path: 'check-daily',
        component: () => import('@/views/table/check-daily'),
        name: 'checkdaily',
        meta: {
          title: '下单列表',
          icon: 'tab',
        }
      }]
  },
  {//商品展示
    path: '/detail',
    component: Layout,
    redirect: '/detail/detailinfo',
    name: 'detail',
    meta: {
      title: '商品展示',
      icon: 'theme',
    },
    children: [
      {//日常工作
        path: 'detailinfo',
        component: () => import('@/views/detail/detailinfo'),
        name: 'detailinfo',
        meta: {
          title: '商品展示',
        }
      }]
  },
  {
    path: '/info',
    component: Layout,
    redirect: '/info/home',
    name: 'info',
    meta: {
      title: '用户信息',
      icon: 'theme'
    },
    children: [
        {
          path: 'news',
          component: () => import('@/views/info/news'),
          name: 'news',
          meta: { title: '用户列表' ,icon: 'wechat',}
        },
      ]
    },
    {//积分规则
      path: '/rule',
      component: Layout,
      redirect: '/IntegralRule/rule',
      name: 'detaruleil',
      meta: {
        title: '积分规则',
        icon: 'link',
      },
      children: [
        {//日常工作
          path: 'IntegralRule',
          component: () => import('@/views/IntegralRule/Rule'),
          name: 'IntegralRule',
          meta: {
            title: '积分规则',
          }
        }]
    },
    {
      path: '/setup',
      component: Layout,
      redirect: '/address',
      name: 'setup',
      meta: {
        title: '系统设置',
        icon: 'password'
      },
      children: [
         {//修改密码
          path: 'password',
          component: () => import('@/views/password/password'),
          name: 'password-update',
          meta: {
            title: '修改密码',icon: 'edit',
          }
        },
      ]
    },
]

// 最低权限路由
export const generalRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/auth-redirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: '/dashboard',
        component: () => import('@/views/dashboard/index'),
        name: 'Dashboard',
        meta: { title: '首页', icon: 'dashboard', affix: true }
      },
    ]
  },
  {//商品信息
    path: '/manage',
    component: Layout,
    redirect: '/permission/page',
    alwaysShow: true, 
    name: 'manage',
    meta: {
      title: '商品信息',
      icon: 'guide',
    },
    children: [
      {//日常工作
        path: 'travel-search',
        component: () => import('@/views/permission/travel-search'),
        name: 'travelsearch',
        meta: {
          title: '商品列表',
          icon: 'star',
        }
      }]
  },{
      path: '/setup',
      component: Layout,
      redirect: '/address',
      name: 'setup',
      meta: {
        title: '系统设置',
        icon: 'password'
      },
      children: [
         {//修改密码
          path: 'password',
          component: () => import('@/views/password/password'),
          name: 'password-update',
          meta: {
            title: '修改密码',icon: 'edit',
          }
        },
      ]
    },
]

export const asyncRoutes = [
  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

// let flag = JSON.parse(sessionStorage.getItem("forest_user_info", true))["managerFlag"];
var createRouter = () => new Router({
  mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
  // routes: flag === '2'? generalRoutes:constantRoutes  // 路由菜单权限控制(与Vuex路由状态文件permission.js匹配)
})
// }
const router = createRouter()

export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
