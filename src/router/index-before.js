import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'
 
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/screen',
    component: () => import('@/views/screen'),
    hidden: true
},
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/auth-redirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: '/dashboard',
        component: () => import('@/views/dashboard/index'),
        name: 'Dashboard',
        meta: { title: '首页', icon: 'dashboard', affix: true }
      },
    ]
  },
  {//信息发布
    path: '/info',
    component: Layout,
    redirect: '/info/home',
    alwaysShow: true, 
    name: '信息发布',
    meta: {
      title: '信息发布',
      icon: 'theme',
      roles: ['admin']
    },
    children: [
      {//新闻动态
        path: 'news',
        component: () => import('@/views/info/news'),
        name: 'news',
        meta: {
          title: '新闻动态',
          icon: 'wechat',
          roles: ['admin',]
        }
      },
      {//宣传视频
        path: 'video',
        component: () => import('@/views/info/video'),
        name: 'video',
        meta: {
          title: '宣传视频',
          icon: 'peoples',
          roles: ['admin',]
        }
      },
      {//通知公告
        path: 'notice',
        component: () => import('@/views/info/notice'),
        name: 'notice',
        meta: {
          title: '通知公告',
          icon: 'tab',
          roles: ['admin',]
        }
      },
      {//政策法规
        path: 'law',
        component: () => import('@/views/info/law'),
        name: 'law',
        meta: {
          title: '政策法规',
          icon: 'zip',
          roles: ['admin',]
        }
      },
    ]
  },
  {//定位管理
    path: '/manage',
    component: Layout,
    redirect: '/permission/page',
    alwaysShow: true, 
    name: 'manage',
    meta: {
      title: '定位管理',
      icon: 'guide',
      roles: ['admin']
    },
    children: [
      {//日常工作
        path: 'travel-search',
        component: () => import('@/views/permission/travel-search'),
        name: 'travelsearch',
        meta: {
          title: '轨迹查询',
          icon: 'star',
          roles: ['admin',]
        }
      },
      {
        path: 'staff-location',
        component: () => import('@/views/permission/staff-location'),
        name: 'stafflocation',
        meta: {
          title: '人员位置',
          icon: 'user',
        }
      }]
  },
  {
    path: '/work',
    component: Layout,
    redirect: '/table/complex-table',
    name: 'work',
    meta: {
      title: '日常工作',
      icon: 'table'
    },
    children: [
        {
          path: 'security-report',
          component: () => import('@/views/table/dynamic-table/security-report'),
          name: 'securityreport',
          meta: { title: '隐患上报' ,icon: 'link',}
        },
        {
          path: 'check-daily',
          component: () => import('@/views/table/check-daily'),
          name: 'DragTable',
          meta: { title: '巡山日志' ,icon: 'language',}
        },
        {
          path: 'Urgent-task',
          component: () => import('@/views/table/Urgent-task'),
          name: 'InlineEditTable',
          meta: { title: '紧急任务' ,icon: 'example',}
        },
        {
          path: 'once-alarm',
          component: () => import('@/views/table/once-alarm'),
          name: 'ComplexTable',
          meta: { title: '一键报警' ,icon: 'component',}
        }
      ]
    },
    {
      path: '/setup',
      component: Layout,
      redirect: '/address',
      name: 'setup',
      meta: {
        title: '系统设置',
        icon: 'password'
      },
      children: [
        {//通讯录
            path: 'address',
            component: () => import('@/views/addressbook/addresslist'),
            name: 'address-list',
            meta: {
              title: '通讯录' ,icon: 'clipboard',
            }
          },
         {//修改密码
          path: 'password',
          component: () => import('@/views/password/password'),
          name: 'password-update',
          meta: {
            title: '修改密码',icon: 'edit',
          }
        },
        {
          path: 'login-count',
          component: () => import('@/views/permission/role'),
          name: 'logincount',
          meta: {
            title: '登录统计',icon: 'education',
            roles: ['admin']
          }
        }]
    },
]
export const asyncRoutes = [
  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
