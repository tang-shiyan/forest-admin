import Vue from 'vue'

import Cookies from 'js-cookie'

import moment from 'moment'

import BaiduMap from 'vue-baidu-map'

import 'normalize.css/normalize.css' // a modern alternative to CSS resets

import Element from 'element-ui'
import vueParticles from 'vue-particles'
// import VueAudio from 'vue-audio-better'
import './styles/element-variables.scss'

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'
import Config from './config'
import './icons' // icon
import './permission' // permission control
import './utils/error-log' // error log

import * as filters from './filters' // global filters


Vue.use(Element, {
  size: Cookies.get('size') || 'medium', // set element-ui default size
})
Vue.use(vueParticles)
// Vue.use(VueAudio)

//引入地图
Vue.use(BaiduMap, {
  ak: 'WsRAdCOoCKhBz11naDVWarl4'
});

// register global utility filters
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

Vue.config.productionTip = false
Vue.prototype.$Moment = moment
Vue.prototype.$Config = Config
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
