import request from '@/utils/request'
//大屏-护林员履职
export function bigscreenperformance(data) {
  return request({
    url: '/bigscreen/performance',
    method: 'post'
  })
}

//大屏-工资情况
export function bigscreensalary(data) {
    return request({
      url: '/bigscreen/salary',
      method: 'post'
    })
  }

  //大屏-基础信息分析-性别结构
export function bigscreenpersonSexs(data) {
    return request({
      url: '/bigscreen/personSexs',
      method: 'post'
    })
  }

    //大屏-基础信息分析-年龄结构
export function bigscreenpersonAges(data) {
    return request({
      url: '/bigscreen/personAges',
      method: 'post'
    })
  }

      //大屏-基础信息分析-人员分布
export function bigscreenpersonDistrib(data) {
    return request({
      url: '/bigscreen/personDistrib',
      method: 'post'
    })
  }