import request from '@/utils/request'


//随手拍信息列表
export function takePicturesList(data) {
  return request({
    url: '/super/slap/list',
    method: 'post',
    data
  })
}
//随手拍信息审核
export function takePicturesAudit(data) {
  return request({
    url: '/super/slap/audit',
    method: 'post',
    data
  })
}

