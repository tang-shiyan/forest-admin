import request from '@/utils/request'


//商品兑换记录列表:管理端
export function goodsbuyrecord(data) {
  return request({
    url: '/super/goodsbuyrecord/buyrecord',
    method: 'post',
    data
  })
}
//商品退货
export function goodsbuyrecordcancel(data) {
  return request({
    url: '/super/goodsbuyrecord/cancel',
    method: 'post',
    data
  })
}

//商品兑换核销
export function goodsbuyrecordconfirm(data) {
  return request({
    url: '/super/goodsbuyrecord/confirm',
    method: 'post',
    data
  })
}

