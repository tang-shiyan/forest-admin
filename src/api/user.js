import request from '@/utils/request'
//登录获取用户信息
export function login(data) {
  return request({
    url: '/super/user/backLogin',
    method: 'post',
    data
  })
}

 //退出登录
export function logout() {
  return request({
    url: '/sys/user/logout',
    method: 'get'
  })
}
//修改密码
export function changePassword(data) {
  return request({
    url: '/super/user/updatePassword',
    method: 'post',
    data
  })
}

//通讯录-组织架构列表查询
export function sysdepts() {
  return request({
    url: '/sys/depts',
    method: 'get',
  })
}

//通讯录-通讯录列表查询
export function sysusers(data) {
  return request({
    url: '/sys/users',
    method: 'post',
    data
  })
}

//用户管理-根据条件查询用户信息
export function sysgetUserInfo(data) {
  return request({
    url: '/sys/getUserInfo',
    method: 'post',
    data
  })
}

//用户维护-组织列表创建
export function sysdept(data) {
  return request({
    url: '/sys/dept',
    method: 'post',
    data
  })
}


//用户维护-APP及WEB用户录入
export function sysuser(data) {
  return request({
    url: '/sys/user',
    method: 'post',
    data
  })
}