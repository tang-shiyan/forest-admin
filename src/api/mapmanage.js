import request from '@/utils/request'
//定位管理-登录统计
export function sysloginLog(data) {
  return request({
    url: '/sys/logs/loginLog',
    method: 'post',
    data
  })
}

//商品列表
export function getGoodsList(data) {
  return request({
    url: '/super/goods/list',
    method: 'post',
    data
  })
}

//商品删除
export function delterGoods(data) {
  return request({
    url: '/super/goods/delete',
    method: 'DELETE',
    data
  })
}

//商品发布
export function addGoods(data) {
  return request({
    url: '/super/goods/add',
    method: 'POST',
    data
  })
}

//商品兑换
export function exchangeGoods(data) {
  return request({
    url: '/super/goods/exchange',
    method: 'POST',
    data 
  })
}
//商品兑换
export function updateByIdGoods(data) {
  return request({
    url: '/super/goods/updateById',
    method: 'POST',
    data
  })
}




//定位管理-个人轨迹查询
export function busiMytralistByPage(data) {
  return request({
    url: '/busiMytra/listByPage',
    method: 'post',
    data
  })
}

//定位管理-人员位置
export function busiMytrapersonPos(data) {
  return request({
    url: '/busiMytra/personPos',
    method: 'post',
    data
  })
}