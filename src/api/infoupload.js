import request from '@/utils/request'
//信息发布-查询通知公告列表
export function sysContentlistByPage(data) {
  return request({
    url: '/sysContent/listByPage',
    method: 'post',
    data
  })
}

//用户列表
export function getUserList(data) {
    return request({
      url: '/super/user/list',
      method: 'post',
      data
    })
  }
//用户导出
export function exportUsers() {
  return request({
    url: '/super/user/exportUsers',
    method: 'get',
  })
}

//新增用户
export function addUser(data) {
  return request({
    url: '/super/user/addUser',
    method: 'post',
    data
  })
}

//用户修改
export function updateUser(data) {
  return request({
    url: '/super/user/updateUser',
    method: 'post',
    data
  })
}


  //文件上传
export function sysFilesupload(data) {
    return request({
      url: '/super/upload/imgUpload',
      method: 'post',
      data
    })
  }
  //用户批量导入
  export function userFilesupload(data) {
    return request({
      url: '/super/user/importUsers',
      method: 'post',
      data
    })
  }
  
//删除用户
export function deleteUser(data) {
  return request({
    url: '/super/user/deleteUser?userId='+data,
    method: 'get',
  })
}

//调整用户积分
export function superIntegralEntry(data) {
  return request({
    url: '/super/integral/entry',
    method: 'post',
    data
  })
}

//获取积分列表
export function superIntegralBackList(data) {
  return request({
    url: '/super/integral/backList',
    method: 'post',
    data
  })
}