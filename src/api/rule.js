import request from '@/utils/request'

//积分规则列表
export function rulequery(data) {
  return request({
    url: '/super/rule/query',
    method: 'post',
    data
  })
}


//积分规则编辑
export function rulesubmit(data) {
  return request({
    url: '/super/rule/submit',
    method: 'POST',
    data
  })
}

