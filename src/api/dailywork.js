import request from '@/utils/request'
//紧急任务-紧急任务查询
export function slapaudit(data) {
  return request({
    url: '/slap/audit',
    method: 'post',
    data
  })
}

//巡山日志查询
export function busiPatrollistByPage(data) {
  return request({
    url: '/busiPatrol/listByPage',
    method: 'post',
    data
  })
}

//一键报警-一键报警列表查询
export function busiAlarmlistByPage(data) {
  return request({
    url: '/busiAlarm/listByPage',
    method: 'post',
    data
  })
}

//隐患上报-隐患上报历史记录查询
export function busiDangerlistByPage(data) {
  return request({
    url: '/busiDanger/listByPage',
    method: 'post',
    data
  })
}

//一键报警-阅读状态提交
export function busiAlarmread(data) {
  return request({
    url: '/busiAlarm/read',
    method: 'put',
    data
  })
}

//巡山日志-阅读状态提交
export function busiPatrolread(data) {
  return request({
    url: '/busiPatrol/read',
    method: 'put',
    data
  })
}

//隐患上报-阅读状态提交
export function busiDangerread(data) {
  return request({
    url: '/busiDanger/read',
    method: 'put',
    data
  })
}

//一键报警-数据删除
export function busiAlarmdelete(data) {
  return request({
    url: '/busiAlarm/delete',
    method: 'delete',
    data
  })
}

//紧急任务-数据删除
export function busiUrgentdelete(data) {
  return request({
    url: '/busiUrgent/delete',
    method: 'delete',
    data
  })
}


//巡山日志-数据删除
export function busiPatroldelete(data) {
  return request({
    url: '/busiPatrol/delete',
    method: 'delete',
    data
  })
}

//隐患上报-数据删除
export function busiDangerdelete(data) {
  return request({
    url: '/busiDanger/delete',
    method: 'delete',
    data
  })
}


//紧急任务-新建紧急任务
export function busiUrgentadd(data) {
  return request({
    url: '/busiUrgent/add',
    method: 'post',
    data
  })
}

