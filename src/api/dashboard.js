import request from '@/utils/request'
//首页-工作数据统计饼状图
export function syshomeworkStatistics(data) {
  return request({
    url: '/sys/home/workStatistics',
    method: 'get'
  })
}


//首页-用户在线情况
export function sysonlinestatus(data) {
  return request({
    url: '/sys/onlinestatus',
    method: 'get'
  })
}
