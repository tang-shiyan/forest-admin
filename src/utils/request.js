import axios from 'axios'
import { MessageBox, Message } from 'element-ui'
import store from '@/store'
import { getToken, removeToken } from '@/utils/auth'
import VueRouter from 'vue-router'
// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API,
  // baseURL: process.env.VUE_APP_BASE_API,
  // baseURL: process.env.ENV==='production'?'http://supermarket.atomic.org.cn':'',
  timeout: 3000
})

// request interceptor
service.interceptors.request.use(
  config => {
    if (store.getters.token) {
      config.headers['authorization'] = getToken()
    }
    // let user=sessionStorage.getItem('forest_user_info') 
    // let userInfo =JSON.parse(user)
    // if(userInfo!=null&&userInfo!=""){
    //   config.data['xzqhdm']=userInfo.xzqhdm
    // }
    return config
  },
  error => {
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

service.interceptors.response.use(

  response => {
    const res = response.data
    console.log(res,'=>>>>>>>')
    if (!res.data && res.status !== '00'){
      Message({
        message: res.msg || 'Error',
        type: 'error',
        duration: 30 * 100
      })
      return Promise.reject(new Error(res.msg || 'Error'))
    }
    // 根据后台返回 正常状态码为0 

    if (res.status !== '00') {
      Message({
        message: res.msg || 'Error',
        type: 'error',
        duration: 30 * 100
      })

      // 登录信息过期  - 列子 依据后台返回为标准
      if (res.code === 401001) {
        // to re-login
        //我太卡了 你在这里写 跳转到登录并 清cookie 调用removeToken
        Message({
          message: res.msg,
          type: 'error',
          duration: 30 * 100
        })
        removeToken()
        sessionStorage.removeItem('hasLogin')
        window.location.reload();
      }
      return Promise.reject(new Error(res.message || 'Error'))
    } else {
      return res
    }
  },
  error => {
    console.log('err' + error)
    Message({
      message: error.message,
      type: 'error',
      duration: 30 * 100
    })
    return Promise.reject(error)
  }
)

export default service
