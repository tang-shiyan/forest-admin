//数据来源
export const formData = {
    laborforce: [ //劳动力分析: "乌沙社区","窑上村","牛蒡子村","抹脚村","磨舍村","普梯村","岔江村","纳姑村","大兴寨村",
        {
            type: '男',
            wusha: 310,
            yaoshang: 302,
            niubangzi:301,
            mojiao:334,
            moshe:490,
            puti:330,
            chajiang:320,
            nagu:390,
            daxingzai:330
        },
        {
            type: '女',
            wusha: 120,
            yaoshang: 132,
            niubangzi:101,
            mojiao:134,
            moshe:90,
            puti:330,
            chajiang:230,
            nagu:210,
            daxingzai:390
        },
        {
            type: '有劳动力',
            wusha: 220,
            yaoshang: 182,
            niubangzi:191,
            mojiao:234,
            moshe:290,
            puti:330,
            chajiang:310,
            nagu:390,
            daxingzai:330
        },
        {
            type: '无劳动力',
            wusha: 150,
            yaoshang: 212,
            niubangzi:201,
            mojiao:154,
            moshe:190,
            puti:330,
            chajiang:410,
            nagu:390,
            daxingzai:330
        }
        ],
    skills: [ //参加技能培训情况："水工", "电工", "种植能手", "木工", "电焊工", "其它培训"
        200, 210, 220, 230, 240, 150
    ],
    incomsource: {  //收入来源分析
        migrant_workers: 120,   //务工收入
        farming: 80    //务农收入
    },
    medical: [   //医疗参保情况： "乌沙社区","窑上村","牛蒡子村","抹脚村","磨舍村","普梯村","岔江村","纳姑村","大兴寨村"
        130, 200, 150, 80, 70, 110, 130, 70, 110
    ],
    livestock: [  //乌沙镇畜牧和家禽养殖动态情况: "牛（头）","羊（头）","马（头）","生猪（头）","鸭（羽）","鹅（羽）","鸡（羽）","水生鱼（条）",
        120, 200, 150, 80, 70, 110, 130, 90
    ],
    statistics: {  //统计 大兴寨村，磨舍村，纳姑村，抹脚村，普梯村，乌沙社区，岔江村，牛蒡字村
        sum_person:[80,450,230, 120, 200, 150,320,130],
        activation_person:[30,120,250,190,120,180,200,100]
    }

}
